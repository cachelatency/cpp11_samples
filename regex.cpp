/* vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab: */

// see http://cachelatency.com/cpp11_regex

// *****************************************************************************
// ********************************* WARNING ***********************************
// The latest stable version of gcc available at the time of writing this file
// is 4.8. If we compile this file with gcc version 4.8 (or any earlier
// version), we might be able to compile and run this file (seeing no warnings
// or errors - even at runtime). However, gcc does not yet implement the regex
// part of the C++11 library in libstdc++. So, though it runs, we will get
// incorrect/undefined results from the regex calls. I don't know which version
// of gcc will eventually ship with an implemntation of <regex> in the library.
//
// Therefore, unlike most of the other samples in this repository, this one is
// tested with clang3.4 with libc++. For help on installing clang, please see
// the link above. We could always try it with any other compiler which
// implements C++11 features and the regex part of the library.
//
// In short: This WON'T work with GCC and libstdc++. Instead, please try
// clang3.3 or higher with libc++ (or any other compiler which ships with
// support for C++11 and a working implementation of <regex>).
//
// One last time, and louder:
//      ************ THIS WON'T WORK WITH GCC-4.8 OR EARLIER ***********
// *****************************************************************************

#include <fstream>
#include <iostream>
#include <regex>

// I usually don't recommend a 'using' directive. However, this code is meant
// to help understand how the regex library works and a profusion of 'std::'
// hurts readability significantly in this file. So, I am making an exception in
// this case. Please do not forget that bringing something into the global
// namespace must be with good reason and is best avoided if you are unsure.
using std::regex;
using std::regex_match;
using std::regex_replace;
using std::regex_search;
using std::smatch;
using std::sregex_iterator;

using std::cout;
using std::string;
using std::vector;

static vector<string>
get_all_lines_in_file(string filename) {
    std::ifstream ifile(filename);

    vector<string> all_lines;
    string line;

    while(std::getline(ifile, line)) {
        all_lines.push_back(line);
    }

    ifile.close();

    return all_lines;
}

int main() {
    vector<string> lines = get_all_lines_in_file("lambda.cpp");

    // regex_search: find a regex in input sequence
    //
    // Print all lines which have '{' followed by '}'
    regex braces_substr("\\{.*\\}");
    for (auto line: lines) {
        // regex_search attempts to finds a match in the input (i.e line)
        bool found = regex_search(line, braces_substr);
        if (found)
            cout << "Found in Line: '" << line << "'\n";
    }
    cout << "\n";

    // regex_match: match regex to input sequence
    //
    // Print each include statement
    // I am assuming the statments follow the convention of:
    // #include{SINGLE SPACE}<{HEADER NAME}>{END OF LINE}
    regex include_statement("#include <.*>");
    for (auto line: lines) {
        // regex_match succeeds only if input (i.e line) exactly matches the
        // given regular expression.
        bool found = regex_match(line, include_statement);
        if (found)
            cout << "Matched Line: '" << line << "'\n";
    }
    cout << "\n";

    // smatch:
    //     - Holds the string that has matched.
    //     - Also lets us access submatches which are marked within parentheses
    //       in the regular expression
    //
    // Find expressions of type a*a + b*b and also print a and b:
    //   match.str(0) -------------------
    //                                  |
    //                   -------------------------------
    //                   |                             |
    regex sqr_expression("(.*)\\*(\\1) \\+ (.*)\\*(\\3)");
    //                     |      |         |      |
    //   match.str(1) ------      |         |      |
    //   match.str(2) -------------         |      |
    //   match.str(3) -----------------------      |
    //   match.str(4) ------------------------------
    for (auto line: lines) {
        smatch match;
        bool found = regex_search(line, match, sqr_expression);
        if (found) {
            cout << "In Line: '" << line << "':\n";
            cout << "    in expression: '" << match.str(0) << "'\n";
            cout << "         squared elements are: " 
                     << "'" << match.str(1) << "'" 
                     << " and "
                     << "'" << match.str(3) << "'"
                     << "'\n";
        }
    }
    cout << "\n";

    // regex_iterator: find all regex matches in input sequence
    // sregex_iterator is just regex_iterator<string>
    //
    // Print all words which begin with an '_' (underscore)
    regex underscore_word("[^\\w](_\\w+)");
    for (auto line: lines) {
        auto word_begin_iter = sregex_iterator(
            line.begin(), line.end(), underscore_word);
        auto word_end_iter = sregex_iterator();

        if (regex_search(line, underscore_word))
            cout << "In Line: '" << line << "':\n";

        for (auto iter = word_begin_iter; iter != word_end_iter; ++iter) {
            smatch match = *iter;
            cout << "    Matched word: '" << match.str(1) << "'\n";
        }
    }
    cout << "\n";

    // regex_replace: replace occurence of a regex with given expression
    //
    // Replace occurences of static_cast<T>(expr) with T(expr)
    regex static_cast_expr("static_cast<(.*)>\\((.*)\\)");
    for (auto line: lines) {
        // Searching and replacing separately is inefficient but this code
        // is mean to show a simple regex_replace call. We could use iterators
        // to make it efficient.
        bool found = regex_search(line, static_cast_expr);
        if (found) {
            string replaced = regex_replace(line, static_cast_expr, "$1($2)");
            cout << "Line: '" << line << "'\n";
            cout << " =>   '" << replaced << "'\n";
        }
    }
}

/*
 * This was compiled and run like this:
 *   clang++ -stdlib=libc++ -nodefaultlibs -lc++ -lm -lc -lgcc_s -lgcc -std=c++11 -Wall -Werror regex.cpp
 *   ./a.out
 *
 * This file reads lambda.cpp and uses the lines from it as input for the
 * regular expressions.
 * For this, we are using lambda.cpp from revision:
 *     77ec8f10abe4cc3fb7bcbd57068b181d153796c4
 *
 * Output presented here is generated by running this file from revision:
 *     86d7f570dad2cbb70734213ff8992ad25686b0d5
 *
 * The output is pasted below between the [BEGIN] and [END] lines.
 *
 * [BEGIN]
Found in Line: '    vec2d(int _x, int _y): x(_x), y(_y) {}'
Found in Line: '    vec2d(const vec2d & src): x(src.x), y(src.y) {}'
Found in Line: '        [] (const vec2d &v1, const vec2d &v2) {return v1.x < v2.x;};'
Found in Line: '      [] (const vec2d &v1, const vec2d &v2) {return v1.y < v2.y;});'

Matched Line: '#include <algorithm>'
Matched Line: '#include <iostream>'
Matched Line: '#include <list>'
Matched Line: '#include <random>'
Matched Line: '#include <string>'

In Line: '    auto mag1_sqr = v1.x*v1.x + v1.y*v1.y;':
    in expression: 'v1.x*v1.x + v1.y*v1.y'
         squared elements are: 'v1.x' and 'v1.y''
In Line: '    auto mag2_sqr = v2.x*v2.x + v2.y*v2.y;':
    in expression: 'v2.x*v2.x + v2.y*v2.y'
         squared elements are: 'v2.x' and 'v2.y''
In Line: '            auto m1 = v1.x*v1.x + v1.y*v1.y;':
    in expression: 'v1.x*v1.x + v1.y*v1.y'
         squared elements are: 'v1.x' and 'v1.y''
In Line: '            auto m2 = v2.x*v2.x + v2.y*v2.y;':
    in expression: 'v2.x*v2.x + v2.y*v2.y'
         squared elements are: 'v2.x' and 'v2.y''
In Line: '            double m1 = dx1*dx1 + dy1*dy1;':
    in expression: 'dx1*dx1 + dy1*dy1'
         squared elements are: 'dx1' and 'dy1''
In Line: '            double m2 = dx2*dx2 + dy2*dy2;':
    in expression: 'dx2*dx2 + dy2*dy2'
         squared elements are: 'dx2' and 'dy2''
In Line: '            return dx*dx + dy*dy;':
    in expression: 'dx*dx + dy*dy'
         squared elements are: 'dx' and 'dy''

In Line: '    vec2d(int _x, int _y): x(_x), y(_y) {}':
    Matched word: '_x'
    Matched word: '_y'
    Matched word: '_x'
    Matched word: '_y'

Line: '    double mean_x = static_cast<double>(sum_x)/random_points.size();'
 =>   '    double mean_x = double(sum_x)/random_points.size();'
Line: '    double mean_y = static_cast<double>(sum_y)/random_points.size();'
 =>   '    double mean_y = double(sum_y)/random_points.size();'
 * [END]
 */

