/* vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab: */

// see http://cachelatency.com/cpp11_random

#include <algorithm>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <vector>

const int MAX_RANGE = 256;
const int BLACK = 0;
const int WHITE = 255;

static void
dump_pgm(std::vector<int> &counts, std::string filename) {
    std::ofstream outfilestream;
    outfilestream.open(filename, std::ios::out);

    if (! outfilestream.is_open()) {
        std::cerr << "Unable to open file for writing: "
                  << filename
                  << std::endl;
    }

    int width = 2 * MAX_RANGE;
    int height = MAX_RANGE;

    outfilestream << "P2\n"
                  << "# " << filename << "\n"
                  << width << " " << height << "\n"
                  << WHITE << "\n";

    for (auto j = 0; j < height; ++j) {
        auto count = counts[j];

        for (auto i = 0; i < count; ++i) {
            outfilestream << BLACK << " ";
        }

        for (auto i = count; i < width; ++i) {
            outfilestream << WHITE << " ";
        }
        outfilestream << "\n";
    }

    outfilestream.close();
}

template <typename EngineType, typename DistributionType>
std::vector<int> get_random_sequence(EngineType engine,
                                     DistributionType distribution) {
    const int count_to_generate = MAX_RANGE*MAX_RANGE;
    std::vector<int> generated(count_to_generate);

    for (auto i = 0; i < count_to_generate; ++i) {
        int new_number = distribution(engine);
        generated[i] = new_number;
    }

    return generated;
}

std::vector<int>
count(std::vector<int> &values) {
    std::vector<int> counts(MAX_RANGE, 0);

    for (auto i = 0u; i < values.size(); ++i) {
        int value = values[i];

        ++counts[value];
    }

    int max = *std::max_element(counts.begin(), counts.end());
    if (max != MAX_RANGE-1) {
        auto scale = static_cast<double>(MAX_RANGE-1)/max;
        for (auto &count: counts) {
            count *= scale;
        }
    }

    return counts;
}

template <typename EngineType, typename DistributionType> void
generate_random_dist_pgm(EngineType engine,
                         DistributionType distribution,
                         std::string image_name) {
    auto generated_numbers = get_random_sequence(engine, distribution);
    auto counts = count(generated_numbers);
    dump_pgm(counts, image_name);
}


int main() {
    std::default_random_engine engine;

    {
        std::uniform_int_distribution<int> distribution(0, MAX_RANGE-1);
        generate_random_dist_pgm(
            engine, distribution, "default_engine_uniform_int_dist.pgm");
    }

    {
        std::binomial_distribution<int> distribution(MAX_RANGE, 0.5);
        generate_random_dist_pgm(
            engine, distribution, "default_engine_binomial_dist.pgm");
    }

    {
        std::geometric_distribution<int> distribution;
        generate_random_dist_pgm(
            engine, distribution, "default_engine_geometric_dist.pgm");
    }

    {
        std::poisson_distribution<int> distribution(128.0);
        generate_random_dist_pgm(
            engine, distribution, "default_engine_poisson_dist.pgm");
    }

    {
        std::vector<int> weights(MAX_RANGE);
        for (auto i = 0; i < MAX_RANGE; ++i) {
            weights[i] = i;
        }

        std::discrete_distribution<int> distribution(
            weights.begin(), weights.end());
        generate_random_dist_pgm(
            engine, distribution, "default_engine_discrete_dist.pgm");
    }
}

