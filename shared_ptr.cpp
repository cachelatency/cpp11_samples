/* vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab: */

// see: http://cachelatency.com/cpp11_shared_ptr

#include <iostream>
#include <memory>
#include <list>

using std::cout;
using std::endl;
using std::list;
using std::shared_ptr;

class Resource {
public:
    // You could imagine we allocate memory, open a file handle, acquire a lock
    // or acquire some other such resource.
    Resource() {
        id = count;
        cout << "Resource Acquired: " << id << endl;
        ++count;
    }

    // We would be freeing the resource here
    ~Resource() { cout << "Resource Released: " << id << endl; }
    void use () { cout << "Resource used: " << id << endl; }

private:
    // We have these only to be able to distinguish one Resource object from
    // another. We could have printed the value of the 'this' pointer but
    // having A, B, C etc. produces more readable output.
    char id;
    static int count;
};
int Resource::count = 'A';

class ResourceUser {
public:
    ResourceUser(shared_ptr<Resource> res_): res(res_) { }
    void use_resource() { res->use(); }

private:
    shared_ptr<Resource> res;
};

// The output produced by this program is given at the end of this file
int main() {
    list<ResourceUser> users;

    // We create ten resource users and four resources are shared among them.
    // Resource Users 0,1     use Resource A;
    // Resource Users 2, 3, 4 use Resource B;
    // Resource Users 5, 6, 7 use Resource C;
    // Resource users 8, 9    use Resource D
    shared_ptr<Resource> res(new Resource);
    for (auto i = 0; i < 10; ++i) {
        // create a new resource for i = 2, 5, 8
        if (i % 3 == 2) {
            // this has the same effect as:
            // res = shared_ptr<Resource> (new Resource);
            res.reset(new Resource);
        }

        users.push_back(ResourceUser(res));
    }

    for (auto iter = users.begin(); iter != users.end(); ++iter) {
        iter->use_resource();
    }

    // Here, we see that until Resource Users 0 and 1 are destroyed, Resource A
    // is not released. Likewise until the corresponding Resource Users are not
    // released, the Resources are not released.
    // shared_ptr maintains a reference count and as each shared_ptr pointing to
    // the same resource gets destroyed, the count is decremented. When the
    // count reaches 0, it calls delete on the raw pointer.
    auto count = 0;
    for (auto iter = users.begin(); iter != users.end();) {
        cout << "Remoing resource user: " << count++ << endl;
        users.erase(iter++); // iterating this way to keep the loop valid
    }
}
/* These lines (except the comment closing line) are the output of this program:
Resource Acquired: A
Resource Acquired: B
Resource Acquired: C
Resource Acquired: D
Resource used: A
Resource used: A
Resource used: B
Resource used: B
Resource used: B
Resource used: C
Resource used: C
Resource used: C
Resource used: D
Resource used: D
Remoing resource user: 0
Remoing resource user: 1
Resource Released: A
Remoing resource user: 2
Remoing resource user: 3
Remoing resource user: 4
Resource Released: B
Remoing resource user: 5
Remoing resource user: 6
Remoing resource user: 7
Resource Released: C
Remoing resource user: 8
Remoing resource user: 9
Resource Released: D
*/
