/* vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab: */

// see http://cachelatency.com/cpp11_lambdas

#include <algorithm>
#include <iostream>
#include <list>
#include <random>
#include <string>

struct vec2d {
    vec2d(int _x, int _y): x(_x), y(_y) {}
    vec2d(const vec2d & src): x(src.x), y(src.y) {}
    const int x;
    const int y;
};

std::string to_string(const vec2d &vec) {
    return "(" + std::to_string(vec.x) + ", " + std::to_string(vec.y) + ")";
}

using std::to_string;
template <typename T> static void
print_list(const std::list<T> & l) {
    for (const auto & item: l) {
        std::cout << to_string(item) << " ";
    }
    std::cout << "\n";
}

bool is_x_less(const vec2d &v1, const vec2d &v2) {
    return v1.x < v2.x;
}

bool is_y_less(const vec2d &v1, const vec2d &v2) {
    return v1.y < v2.y;
}

bool is_magnitude_less(const vec2d &v1, const vec2d &v2) {
    auto mag1_sqr = v1.x*v1.x + v1.y*v1.y;
    auto mag2_sqr = v2.x*v2.x + v2.y*v2.y;
    return mag1_sqr < mag2_sqr;
}

int main() {
    // The output of this program is pasted at the end of this file
    const auto RAND_SEED = 1729;
    std::default_random_engine engine(RAND_SEED);
    std::uniform_int_distribution<int> distribution(0, 9);

    std::list<vec2d> random_points;
    const auto NUM_POINTS = 5;
    for (auto i = 0; i < NUM_POINTS; ++i) {
        int x = distribution(engine);
        int y = distribution(engine);

        random_points.push_back(vec2d(x, y));
    }

    // Let's see how the list looks
    std::cout << "initial list:             ";
    print_list(random_points);
    std::cout << "\n";

    // Now let's sort the points according to 'x'
    random_points.sort(is_x_less);
    std::cout << "increasing x coordinate:  ";
    print_list(random_points);

    // Now let's sort the points according to 'y'
    random_points.sort(is_y_less);
    std::cout << "increasing y coordinate:  ";
    print_list(random_points);

    // Now let's sort the points according to magnitude
    random_points.sort(is_magnitude_less);
    std::cout << "increasing magnitude:     ";
    print_list(random_points);

    std::cout << "\n";
    // the same as above now done using lambdas
    auto is_x_less_lambda_version =
        [] (const vec2d &v1, const vec2d &v2) {return v1.x < v2.x;};
    random_points.sort(is_x_less_lambda_version);
    std::cout << "increasing x coordinate:  ";
    print_list(random_points);

    random_points.sort(
      [] (const vec2d &v1, const vec2d &v2) {return v1.y < v2.y;});
    std::cout << "increasing y coordinate:  ";
    print_list(random_points);

    random_points.sort(
        [] (const vec2d &v1, const vec2d &v2) {
            auto m1 = v1.x*v1.x + v1.y*v1.y;
            auto m2 = v2.x*v2.x + v2.y*v2.y;
            return m1 < m2;
        });
    std::cout << "increasing magnitude:     ";
    print_list(random_points);


    std::cout << "\n";
    // Without using a lambda, this would need us to make a functor class which
    // holds two members for accumulating the sum.
    auto sum_x = 0;
    auto sum_y = 0;
    for_each(random_points.begin(), random_points.end(),
        // This is how we 'capture' sum_x and sum_y as references
        [&sum_x, &sum_y] (const vec2d &v) {
            sum_x += v.x;
            sum_y += v.y;
        });
    std::cout << "sum_x, sum_y = " << sum_x << ", " << sum_y << "\n";

    double mean_x = static_cast<double>(sum_x)/random_points.size();
    double mean_y = static_cast<double>(sum_y)/random_points.size();
    std::cout << "mean_x, mean_y = " << mean_x << ", " << mean_y << "\n";
    std::cout << "\n";

    // let's sort based on distance to mean point:
    //
    // If we don't use a lambda here, we need to have a class that takes mean_x
    // and mean_y as arguments to the cosntructor and use an instance of it as a
    // functor which compares two points based on distance to mean.
    random_points.sort(
        // This is how we 'capture' mean_x and mean_y as values
        [mean_x, mean_y] (const vec2d &v1, const vec2d &v2) {
            double dx1 = v1.x - mean_x;
            double dy1 = v1.y - mean_y;
            double m1 = dx1*dx1 + dy1*dy1;

            double dx2 = v2.x - mean_x;
            double dy2 = v2.y - mean_y;
            double m2 = dx2*dx2 + dy2*dy2;

            return m1 < m2;
        });
    std::cout << "increasing distance to mean: ";
    print_list(random_points);

    // this is to ensure we don't have same order as above:
    random_points.sort(is_x_less);
    std::cout << "increasing x:                ";
    print_list(random_points);

    // This is the sort based on distance to mean refactored for a bit more
    // readability
    auto distance_to_mean = [mean_x, mean_y] (const vec2d &v) {
            double dx = v.x - mean_x;
            double dy = v.y - mean_y;
            return dx*dx + dy*dy;
        };

    random_points.sort(
        [&distance_to_mean] (const vec2d &v1, const vec2d &v2) {
            return distance_to_mean(v1) < distance_to_mean(v2);
        });

    std::cout << "increasing distance to mean: ";
    print_list(random_points);
}
/*
 * The output of this file on my computer is between the [BEGIN] and [END]
 * marks (not including the marks).
 *
 * The values may differ on another system since I am using the
 * default_random_engine and its behaviour is implementation defined.
 * However, the sorting will work fine.
 *
 * Here's the output:
[BEGIN]
initial list:             (0, 4) (4, 0) (1, 5) (3, 7) (5, 0) 

increasing x coordinate:  (0, 4) (1, 5) (3, 7) (4, 0) (5, 0) 
increasing y coordinate:  (4, 0) (5, 0) (0, 4) (1, 5) (3, 7) 
increasing magnitude:     (4, 0) (0, 4) (5, 0) (1, 5) (3, 7) 

increasing x coordinate:  (0, 4) (1, 5) (3, 7) (4, 0) (5, 0) 
increasing y coordinate:  (4, 0) (5, 0) (0, 4) (1, 5) (3, 7) 
increasing magnitude:     (4, 0) (0, 4) (5, 0) (1, 5) (3, 7) 

sum_x, sum_y = 13, 16
mean_x, mean_y = 2.6, 3.2

increasing distance to mean: (1, 5) (0, 4) (4, 0) (3, 7) (5, 0) 
increasing x:                (0, 4) (1, 5) (3, 7) (4, 0) (5, 0) 
increasing distance to mean: (1, 5) (0, 4) (4, 0) (3, 7) (5, 0) 
[END]
 *
 */
