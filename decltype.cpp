/* vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab: */
// see: http://cachelatency.com/cpp11_decltype
// even more details at:
//     http://thbecker.net/articles/auto_and_decltype/section_01.html

// We don't know what multiply returns. So we use the expression to deduce the
// return type (also this serves as an example of the trailing return type
// declaration syntax).
// In this case, there is no alternative to decltype
// Also we can't do: decltype(x * y) multiply(T1 x, T2 y) { // ...
// This is because x and y are not declared for decltype to work.
template <typename T1, typename T2>
auto multiply(T1 x, T2 y) -> decltype(x * y) {
    return x * y;
}

int main() {
    int a = 21;
    int b = 2;

    // int c = a * b;   // since int * int is int
    decltype(a * b) c = a * b;

    // int k;
    decltype(b) k;

    // float d = 100.0f;
    decltype(100.0f) d = 100.0f;

    // double e = 0.01
    decltype(0.03) e = 0.01;

    // int g = c * 1    // since the expression c / 0 is not evaluated
    decltype(c / 0) g = c * 1;

    // int &h = g;  // since (c) is an expression that is an lvalue unlike in
                    // the declaration of g where the result is a temporary
                    // which is not an lvalue
    decltype((c)) h = g;

    // decltype(multiply(100.0f, 2)) i = multiply(d, b)
    // float i = multiply(d, b)
    decltype(multiply(d, b)) i = multiply(d, b);
}
