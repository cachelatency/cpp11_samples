/* vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab: */

// see: http://cachelatency.com/cpp11_shared_ptr_revisited

#include <iostream>
#include <memory>

class Queen {
public:
    Queen(int, std::string) {}
    void send_message(std::string) {}
};

int main() {
    {
        {
            Queen *a = new Queen(4, "White Queen");
            std::shared_ptr<Queen> p(a);
        }
        // The above practically is the same as:
        {
            auto p = std::make_shared<Queen>(4, "White Queen");
        }
    }

    {
        int *ptr_num_hats = new int(1729);
        int *ptr_num_soldiers = new int(52);

        const std::shared_ptr<int> p(ptr_num_hats);
        // p.reset(ptr_num_soldiers); // this will not compile: p is const
        ++(*p); // this will work: raw pointer held by p is not pointer to const
    }

    {
        int *const ptr_num_hats = new int(1729);
        int *const ptr_num_soldiers = new int(52);

        std::shared_ptr<const int> p(ptr_num_hats);
        // ++(*p); // this won't compile: p holds raw pointer to const int
        p.reset(ptr_num_soldiers); // this will work: p is not const
    }
}
