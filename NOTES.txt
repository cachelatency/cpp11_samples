Feedback from HN:
- tags are redundant
- shared_ptr
  - give examples of when we need to take away reference from shared_ptr so it
    outlives the shared_ptr
  - mention std::make_shared
  - show example of bare ptr vs shared_ptr to do same thing
  - Show object liftime in RAII using a print in destructor
  - ClassP typedef for shared_ptr<Class>
    (this one isn't all that necessary)
  - shared_ptr with dynamic arrays (shared_ptr<T>(new T[N])
  - introduce use_count to show how it is working
  - mention that we shouldn't use references to shared_ptr and just use it as a
    value type
- std::declval should be part of decltype article
- (demand for) advice on when to use and not use features of C++11
- add 'search' for articles
  (This ought to work. A problem with the current theme on Drupal.)
- elaborate upon 'auto's interaction with 'const' and '&'
- basic snippets like http://cocoadevcentral.com

Feedback from Reddit:
- site is a bit busy
- content should be in centre, it has gone to the right, examples of good layout
  given were: http://qt-project.org/doc/qt-5.0/qtcore/qobject.html,
- needs more details on decltype's lvale, two types of rvalue references
- needs index
- demand for article on "initialisation lists"
  (This is anyway scheduled for later)

Feedback frm Asif:
- "Read More" link is hard to find
- "Put sidebar on right so content gets central focus
- if you have 3 column layout, use 20-60-20 or 25-50-25 proportions for layout
