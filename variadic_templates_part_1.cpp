/* vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab: */

// see http://cachelatency.com/cpp11_variadic_templates_part_1

#include <functional>
#include <iostream>
#include <string>

template <typename T> T
max(T a) {
    return a;
}

template <typename T, typename ...Types> T
max(T head, Types ...body) {
    auto max_body = max(body...);

    return (head > max_body) ? head : max_body;
}

template <typename T> void
print(T a) {
    std::cout << a;
}

template <typename T, typename ...Types> void
print(T head, Types ...body) {
    print(head);
    print(body...);
}

template <typename InitValType, typename OperationType> InitValType
reduce(InitValType initval, OperationType) {
    return initval;
}

template <typename InitValType, typename OperationType, typename HeadType,
          typename ...Types> InitValType
reduce(InitValType initval, OperationType operation, HeadType head,
       Types ...body) {
    return reduce(operation(initval, head), operation, body...);
}

template <typename OperationType, typename HeadType> void
map(OperationType operation, HeadType &head) {
    operation(head);
}

template <typename OperationType, typename HeadType, typename ...Types> void
map(OperationType operation, HeadType &head, Types &...body) {
    map(operation, head);
    map(operation, body...);
}

int main() {
    std::cout << max(4) << "\n";
    std::cout << max(4, 3.0) << "\n";
    std::cout << max(4, 3.0, 'a') << "\n";
    // Uncomment the line below to see the compiler complain
//    std::cout << max(4, 3.0, 'a', "White Rabbit") << "\n";

    print ("max(", 4, ") = ", max(4), "\n");
    print ("max(", 4, ", ", 3.0, ") = ", max(4, 3.0), "\n");
    print ("max(", 4, ", ", 3.0, ", '", 'a', "') = ", max(4, 3.0, 'a'), "\n");

    auto adder = [](int a, int b) {return a + b;};
    auto sum = reduce(0, adder, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
    print("sum = ", sum, "\n");

    auto multiplier = [](int a, int b){return a * b;};
    auto product = reduce(1, multiplier, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1);
    print("product = ", product, "\n");

    int a = 1, b = 2, c = 3, d = 4, e = 5;

    auto comma_join = [](std::string a, int b) {
            return a + (a.length() ? ", " : "") + std::to_string(b);
        };

    auto doubler = [](int &a) { a *= 2; };
    map(doubler, a, b, c, d, e);
    print("a, b, c, d, e = ",
          reduce(std::string(""), comma_join, a, b, c, d, e),
          "\n");

    auto decrementor = [] (int &a) { --a; };
    map(decrementor, e, d, c, b, a);
    print("a, b, c, d, e = ",
          reduce(std::string(""), comma_join, a, b, c, d, e),
          "\n");
}
