/* vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab: */

// see http://cachelatency.com/cpp11_auto

#include <map>
#include <string>
#include <typeinfo>
#include <vector>

static unsigned char * alloc_uchar(unsigned long size) {
    return new unsigned char [size];
}

int main() {
    // int a = 42;
    auto a = 42;

    // double b = 3.7;
    auto b = 3.7;

    // float b_and_a_half = 4.2f
    auto b_and_a_half = 4.2f;

    // const char *c = "Mad Hatter";
    auto c = "Mad Hatter";

    // unsigned d = 42u;
    auto d = 42u;

    std::map<std::string, std::vector<int>> p;
    // std::map<std::string, std::vector<int>>::iterator e = p.begin();
    auto e = p.begin();

    // unsigned char *(*f)(unsigned long) = alloc_uchar;
    auto f = alloc_uchar;

    // unsigned char *g = alloc_uchar(5);
    auto g = alloc_uchar(5);
    delete []g;

    std::vector<unsigned char *(*)(unsigned long)> q{alloc_uchar};
    // std::vector<unsigned char *(*)(unsigned long)>::iterator h = q.end();
    auto h = q.end();

    const int i = 43;
    // int j = i; // j is not const
    auto j = i;

    // const int k = j;
    const auto k = j;

    const int &l = k;
    // int m = l; // m is neither const nor a reference
    auto m = l;

    // int &n = m;
    auto &n = m;

    // double o = 4.0 + n;
    auto o = 4.0 + n;
}
